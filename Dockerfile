FROM python:3

RUN pip3 install aiohttp \
 && mkdir -p /src/pothead
COPY pothead /src/pothead

WORKDIR /src
ENV PYTHONPATH=/src

ENTRYPOINT ["python3", "-m", "pothead.server"]